package com.example.emanu.diplomskiclient;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;

import java.util.Arrays;
import java.util.LinkedList;

public class HomeActivity extends AppCompatActivity {

    private Spinner mSpinner;
    private ArrayAdapter<String> mAdapter;
    private LinkedList<String> mLanguages = new LinkedList<>(Arrays.asList("Srpski", "English", "German", "Italian", "Spanish", "Other..."));
    protected static String sLanguage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mSpinner = findViewById(R.id.languageSpinner);
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, mLanguages);
        mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mAdapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = (String) parent.getItemAtPosition(position);
                if(item.equals("Other..."))
                    showDialog();
                else
                    sLanguage = item;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) { }
        });

    }

    public void showDialog(){
        final EditText editText = new EditText(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        lp.setMargins(50, 5, 50, 10);
        editText.setLayoutParams(lp);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(editText);
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                int position = mLanguages.size() - 1;
                mLanguages.add(position, editText.getText().toString());
                mAdapter.notifyDataSetChanged();
                mSpinner.setSelection(mLanguages.size() - 2);
                sLanguage = editText.getText().toString();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.setMessage("Add other language");
        builder.create().show();
    }

    public void onClickScan(View view) {
        Intent intent = new Intent(this, LoadingScanActivity.class);
        startActivity(intent);
    }
}
